import time
import os
import sys
import logging
from logging import info
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='dev.log')
info('Loading dependencies...')
from PIL import Image
import urllib.request,io
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton, InputMediaPhoto
from telegram.ext import Updater, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters, CallbackQueryHandler
from telegram import Update, Bot
sys.path[0] += r"\\src"
from cards_orm import dbAPI
API = dbAPI('cards.db')
API.initDB()
class ST:
    
    MAIN_KEYBOARD_LIST = 'Мои карточки'
    MAIN_KEYBOARD = [[MAIN_KEYBOARD_LIST]]
    page2_delete = "удалить"
    page2_qr = "сгенерировать штрих код"
    page2 =[[page2_qr],[page2_delete]]
class bot:

    def __init__(self):
        self.bot = Updater('1677727977:AAH1HwjxkYgsJK3k7oV5lCDrfCP52HaEKs8')
        self.dispatcher = self.bot.dispatcher
        self.dispatcher.add_handler(CommandHandler('start', self.start))
        self.dispatcher.add_handler(CommandHandler('help', self.help))
        self.dispatcher.add_handler(MessageHandler(Filters.text('Мои карточки'),self.list_cards))
        self.dispatcher.add_handler(MessageHandler(Filters.regex(r'^\d* [а-яА-Яa-zA-Z]*$'),self.addcard))
        # self.dispatcher.add_handler(CallbackQueryHandler(self.debug, pattern=r'^.*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_cards, pattern=r'^listcards \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_cards, pattern=r'^listcards$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.get_card, pattern=r'^card \d* \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.remove_card, pattern=r'^removecard \d* \d* (?:yes|no)$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.gcode, pattern=r'^shtrih \d*$'))
        # self.dispatcher.add_handler(CallbackQueryHandler(self.get_card, pattern=r'^back \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.delete_message, pattern=r'^delmsg$'))
        #self.dispatcher.add_handler(CallbackQueryHandler(self.proc,pattern=r'^get_user_info_\d*_\d*$'))

    def debug(self, update:Update, context:CallbackContext):
        print(update.callback_query.data)

    def start(self, update:Update, context:CallbackContext):
        uid = update.message.chat.id
        API.getUser(uid)
        update.message.delete()
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
        print('2')
        update.message.reply_text('hello',reply_markup=ReplyKeyboardMarkup(ST.MAIN_KEYBOARD, one_time_keyboard=False, resize_keyboard= True))
    
        update.message.reply_text('Привет! это бот для сохранения карточек\n для получения подробной информации отправте >> /help',reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))

        
    def help(self, update:Update, context:CallbackContext):
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
        update.message.reply_text('это бот для сохранения карточек \n\nдля добавления новой карточки отправте боту сообщение вида:\n"номер название"\n\nПример:\n12345 лента\n\n при возникновении любых впросов пишите >> @Shyam134',reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))
        update.message.delete()
        
    def addcard(self, update:Update, context:CallbackContext):
        uid = update.message.chat.id
        a,b =  update.message.text.split()
        a = str(a)
        API.addCard(uid,a,b)
        update.message.delete()
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
        update.message.reply_text(f'карта {b} добавленна',reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))
        #time.sleep(1)
        #print(update)
        #update.message.delete()
    def list_cards(self, update:Update, context:CallbackContext):
        if update.callback_query:
            uid = update.callback_query.message.chat.id
        else:
            uid = update.message.chat.id
        user_cards = API.getUserCards(uid)
        if user_cards:
            page = 0
            if update.callback_query:
                try:
                    _, readpage = update.callback_query.data.split()
                    page = int(readpage)
                except Exception:
                    pass
            object_in_page = 5
            cards = []
            for i in user_cards[object_in_page*page:object_in_page*page + object_in_page]:
                name = i['name']
                cid = i['id']
                cards.append([InlineKeyboardButton(name, callback_data=f'card {cid} {page}')])
            prevnext = []
            if page > 0:
                prevnext.append(InlineKeyboardButton(f'{page} <<', callback_data=f'listcards {page-1}'))
            if len(user_cards) > object_in_page * (page + 1):
                prevnext.append(InlineKeyboardButton(f'>> {page+2}', callback_data=f'listcards {page+1}'))
            if prevnext:
                cards.append(prevnext)
            cards.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])

            if update.callback_query:
                update.callback_query.message.edit_text('Ваши карточки:') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
                update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(cards,one_time_keyboard=False, resize_keyboard=True))
            else:
                update.message.reply_text("Ваши карточки:", reply_markup=InlineKeyboardMarkup(cards,one_time_keyboard=False, resize_keyboard=True))
                update.message.delete()

        else:
            update.message.delete()
            key = []
            key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
            update.message.reply_text(f'у вас нет карт введите /help',reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))

    def get_card(self, update:Update, context:CallbackContext):
        _, cid, page = update.callback_query.data.split()
        a = API.getcard(cid)
        #update.message.message2.edit_text('Pineapple! 🍍')
        code = a['code']
        name = a['name']
        buttons = [
            [InlineKeyboardButton('Назад', callback_data=f'listcards {page}')],
            [InlineKeyboardButton('сгенерировать штрих код', callback_data=f'shtrih {code}')],
            [InlineKeyboardButton('Удалить', callback_data=f'removecard {cid} {page} no')],
        ]
        update.callback_query.message.edit_text(f'карта {name}\nКод карты: {code}') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(buttons,one_time_keyboard=False, resize_keyboard=True))

    def remove_card(self, update:Update, context:CallbackContext):
        _, cid, page, confirm = update.callback_query.data.split()
        if confirm == 'yes':
            API.removeCard(cid)
            update.callback_query.message.data = ' '.join(f'listcards {page}')
            self.list_cards(update, context)
            return
        a = API.getcard(cid)
        #update.message.message2.edit_text('Pineapple! 🍍')
        name = a['name']
        buttons = [
            [InlineKeyboardButton('Нет', callback_data=f'card {cid} {page}')],
            [InlineKeyboardButton('Да', callback_data=f'removecard {cid} {page} yes')],
        ]
        update.callback_query.message.edit_text(f'Вы действительно хотите удалить карту {name}?') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(buttons,one_time_keyboard=False, resize_keyboard=True))
    
    def gcode(self, update:Update, context:CallbackContext):
        # print(update)
        _, code = update.callback_query.data.split()
        buttons = [
            [InlineKeyboardButton('Закрыть', callback_data=f'delmsg')],
        ]
        image = Image.open(io.BytesIO(urllib.request.urlopen(f'https://barcode.tec-it.com/barcode.ashx?data={code}&code=Code128&translate-esc=on').read()))
        x,y= int(image.size[0]),(image.size[1])
        x = x + 80
        y = y + 80 
        img = Image.new( mode = "RGB", size = (x, y),color = 'white' )
        img.paste(image, (40, 40))
        #tmp_byte_array = io.BytesIO()
        img.save('1.png')
        a = open('1.png', 'rb')
        update.callback_query.message.reply_photo(a, reply_markup=InlineKeyboardMarkup(buttons, one_time_keyboard=False, resize_keyboard=True))
        a.close()
        os.remove('1.png')
    def delete_message(self, update:Update, context:CallbackContext):
        update.callback_query.message.delete()

    def run(self):    
        logging.info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()

if __name__ == "__main__":
        bot = bot()
        bot.run()

