import sqlite3


class dbAPI:
    def __init__(self, filename):
        self.db_file = filename
        self.conn = sqlite3.connect(self.db_file, check_same_thread = False)
        self.c = self.conn.cursor()
    
    def commit(self):
        self.conn.commit()
    
    def queryCommit(self, query, data=None):
        if data:
            self.c.execute(query, data)
        else:
            self.c.execute(query)
        self.commit()
    
    def query(self, query, data=None):
        if data:
            self.c.execute(query, data)
        else:
            self.c.execute(query)
    
    def initDB(self):
        self.queryCommit("""
            CREATE TABLE IF NOT EXISTS user (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                uid TEXT NOT NULL,
                state TEXT DEFAULT 'Main',
                page INTEGER DEFAULT 0
            )
        """)
        self.queryCommit("""
            CREATE TABLE IF NOT EXISTS card (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                code TEXT,
                name TEXT,
                img TEXT,
                user_id INTEGER
            )
        """)
    
    def fetchone(self):
        return self.c.fetchone()
    
    def fetchall(self):
        return self.c.fetchall()
    
    def getUser(self, uid, go=True):
        self.query("SELECT id, state, page FROM user WHERE uid=?", (uid,))
        r = self.fetchone()
        if not r and go:
            self.addUser(uid)
            r = self.getUser(uid, False)
        if r:
            try:
                return {'id': r[0], 'state': r[1], 'page': r[2]}
            except KeyError:
                return r
        return None
    
    def addUser(self, uid):
        self.queryCommit("INSERT INTO user (uid) VALUES (?)", (uid,))
    
    def addCard(self, uid, code, name=None, img=None):
        user = self.getUser(uid)
        self.queryCommit("INSERT INTO card (code, name, img, user_id) VALUES (?, ?, ?, ?)", (code, name, img, user['id']))
    
    def setUserState(self, uid, state, page=0):
        user = self.getUser(uid)
        self.queryCommit("UPDATE user SET state=?, page=? WHERE id=?", (state, page, user['id']))

    def getUserCards(self, uid):
        user = self.getUser(uid)
        self.query("SELECT id, code, name, img FROM card WHERE user_id=?", (user['id'],))
        r = self.fetchall()
        if not r:
            return None
        res = []
        for el in r:
            try:
                res.append({'id': el[0], 'code': el[1], 'name': el[2], 'img': el[3]})
            except KeyError:
                res.append(el)
        return res

    def getcard(self, id):
        self.query("SELECT  code, name, img FROM card WHERE id=?", (id,))
        r = self.fetchone()
        if not r:
            return None
        res = {'code': r[0], 'name': r[1]}
        return res
        
    
    def removeCard(self, id):
        self.queryCommit("DELETE FROM card WHERE id=?", (id,))
    