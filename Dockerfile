FROM python:3.8

WORKDIR /code

RUN mkdir /code/data

ENV PRODUCTION_MODE=True

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY src/ .

CMD [ "python", "./start.py" ]